use std::net::UdpSocket;

use noise_protocol::{patterns, HandshakeState, U8Array, DH};
use noise_rust_crypto::{sensitive::Sensitive, Aes256Gcm, Sha256, X25519};

fn main() {
    let client = std::fs::read("client.key").unwrap();
    let server = std::fs::read("server.pub").unwrap();

    let client: <X25519 as DH>::Key = Sensitive::from_slice(&client);
    let server: <X25519 as DH>::Pubkey = server.try_into().unwrap();

    let mut hs = HandshakeState::<X25519, Aes256Gcm, Sha256>::new(
        patterns::noise_xx(),
        true,
        &[],
        Some(client),
        None,
        Some(server),
        None,
    );

    let socket = UdpSocket::bind("127.0.0.1:0").unwrap();
    socket.connect("127.0.0.1:8080").unwrap();

    let mut buffer = [0; 1024];
    let length = hs.get_next_message_overhead();
    hs.write_message(&[], &mut buffer[..length]).unwrap();
    socket.send(&buffer[..length]).unwrap();
    dbg!(hs.completed());

    let length = socket.recv(&mut buffer).unwrap();
    println!("recv {:x?}", &buffer[..length]);
    hs.read_message(&buffer[..length], &mut []).unwrap();
    dbg!(hs.completed());

    let length = hs.get_next_message_overhead();
    hs.write_message(&[], &mut buffer[..length]).unwrap();
    socket.send(&buffer[..length]).unwrap();
    dbg!(hs.completed());

    let (mut tx, mut rx) = hs.get_ciphers();

    let msg = "helloclient";
    let out_len = msg.len() + 16;
    tx.encrypt(msg.as_bytes(), &mut buffer[..out_len]);
    socket.send(&buffer[..out_len]).unwrap();
    
    let mut buffer2 = [0; 1024];
    let (length, addr) = socket.recv_from(&mut buffer).unwrap();
    let in_len = length - 16;
    rx.decrypt(&buffer[..length], &mut buffer2[..in_len]).unwrap();
    println!("{}", std::str::from_utf8(&buffer2[..in_len]).unwrap());
}
