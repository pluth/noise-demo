use std::fs::OpenOptions;

use noise_protocol::DH;
use noise_rust_crypto::X25519;

fn main() {

    let client = X25519::genkey();
    let server = X25519::genkey();

    let client_pub = X25519::pubkey(&client);
    let server_pub = X25519::pubkey(&server);

    std::fs::write("client.key", client.as_slice()).unwrap();
    std::fs::write("server.key", server.as_slice()).unwrap();

    std::fs::write("client.pub", client_pub.as_slice()).unwrap();
    std::fs::write("server.pub", server_pub.as_slice()).unwrap();
}